from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework import exceptions
import redis
import _pickle as pk

rd = redis.Redis()

def parse_token(request):
    tk_header =  request.META.get("HTTP_AUTHORIZATION", "").lower()
    if not "token" in tk_header:
        raise exceptions.AuthenticationFailed('No token on header')
    tk_header = tk_header.split()
    if len(tk_header) < 2:
        raise exceptions.AuthenticationFailed('Token not valid')
    token = tk_header[1]
    return token

def redis_logout(request):
    token = parse_token(request)
    del_status =  rd.delete(token)
    if del_status:
        rd.delete(request.user.username)
    return del_status

class HybridToken(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = request.META.get("HTTP_AUTHORIZATION").split()[1]
        uid = rd.get(token)
        return (User.objects.get(id=uid), None)

class RedisToken(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = parse_token(request)
        rd_user = rd.get(token)
        if not rd_user:
            raise exceptions.AuthenticationFailed('User not found')
        user = pk.loads(rd.get(token))
        return (user, None)
