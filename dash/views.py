from django.http import Http404, JsonResponse
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from .authentications import RedisToken, rd, redis_logout
import uuid
import  _pickle as pk


class Dash(APIView):
    authentication_classes = [RedisToken,]
    def get(self, request, format=None):
        return JsonResponse({'username': request.user.username})

class Logout(APIView):
    authentication_classes = [RedisToken,]
    def get(self, request, format=None):
        if redis_logout(request):
            return JsonResponse({'info': "logout ok"})
        else:
            return JsonResponse({'info': 'logout failed'})

class Login(APIView):
    permission_classes = [permissions.AllowAny,]
    def post(self, request, format=None):
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username,
                password=password)
        if user is not None:
            last_token = rd.get(user.username)
            if last_token:
                rd.delete(last_token)
                rd.delete(user.username)
            token = uuid.uuid4().hex
            rd.set(token,pk.dumps(user))
            rd.set(user.username,token)
            return JsonResponse({'token': token})
        else:
            return JsonResponse({"error": "user not found"}, status=403)
